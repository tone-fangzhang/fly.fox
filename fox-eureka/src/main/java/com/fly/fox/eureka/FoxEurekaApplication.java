package com.fly.fox.eureka;

import com.fly.fox.common.FoxFrameworkInitialization;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

import org.springframework.context.annotation.Configuration;

@EnableEurekaServer
@SpringBootApplication
@Configuration
public class FoxEurekaApplication {

	public static void main(String[] args) {
		FoxFrameworkInitialization.initPlatformEnvAndArgs(args);
		SpringApplication.run(FoxEurekaApplication.class, args);
		//new SpringApplicationBuilder(FoxEurekaApplication.class).web(true).run(args);
	}
}
