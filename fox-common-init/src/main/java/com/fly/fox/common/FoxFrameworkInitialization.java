package com.fly.fox.common;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 */
public class FoxFrameworkInitialization {
    private static final Logger logger = LoggerFactory.getLogger(FoxFrameworkInitialization.class);

    public static final String PLATFORM_HOME="fox.home";
    public static final String LOG_BASE_PATH = "fox.log.path";
    public static final String BATCH_ENV = "fox.batch.enabled";
    public static final String REGISTER_WITH_EUREKA = "registerWithEureka";
    //public static final String REGISTER_WITH_EUREKA = "eureka.client.registerWithEureka";

    /**
     * 处理平台参数环境
     * @param args
     */
    public static void initPlatformEnvAndArgs(String[] args) {
        if(StringUtils.isBlank(System.getProperty(PLATFORM_HOME))) {
            if(logger.isWarnEnabled()) {
                logger.warn("Env variable \"{}\" is not defined.",PLATFORM_HOME);
                System.out.println("Env variable \"f\" is not defined.");
            }
        }
        
        if (StringUtils.equalsIgnoreCase(System.getProperty(BATCH_ENV), "true")) {
            System.setProperty(REGISTER_WITH_EUREKA, "false");
        } else {
            System.setProperty(REGISTER_WITH_EUREKA, "true");
        }
        
        String env = Env.getProfile(Env.getDefaultEnv());
        boolean hasProfilesActiveArg = false;
        if(args != null && args.length > 0) {
            for (int i = 0; i < args.length; i++ ) {
                String arg = args[i];
                if(StringUtils.startsWith(arg, "--spring.profiles.active=")) {
                    env = arg.split("=")[1];
                    if(StringUtils.isNotBlank(env)) {
                        hasProfilesActiveArg = true;
                    }
                    break;
                }
            }
        }
        if(! hasProfilesActiveArg) {
            if(StringUtils.isBlank(System.getProperty("spring.profiles.active"))) {
                System.setProperty("spring.profiles.active", StringUtils.isEmpty(env)?env: Env.getProfile(Env.getDefaultEnv()));
                if(logger.isWarnEnabled()) {
                    logger.warn("Param \"spring.profiles.active\" is not exists. set default evn \"{}\"", Env.getProfile(Env.getDefaultEnv()));
                }
            }
        }
        if(StringUtils.isBlank(System.getProperty(LOG_BASE_PATH))) {
            System.setProperty(LOG_BASE_PATH, "../logs");
        }
        
//        Properties properties = System.getProperties();
//        if(logger.isInfoEnabled()) {
//            StringBuilder propertiesSb = new StringBuilder();
//            properties.stringPropertyNames().forEach(key -> propertiesSb.append(key).append(" : ")
//                .append(System.getProperty(key)).append(ResourceUtils.ENTER_NEW_LINE));
//            logger.info("Show system properties: {} {}", ResourceUtils.ENTER_NEW_LINE, propertiesSb.toString());
//            propertiesSb.delete(0, propertiesSb.length());
//            if(args != null) {
//                Stream.of(args).parallel().forEach(arg -> propertiesSb.append(arg).append(ResourceUtils.ENTER_NEW_LINE));
//                logger.info("Show start server args: {} {}",ResourceUtils.ENTER_NEW_LINE, propertiesSb.toString());
//            }
//            propertiesSb.delete(0, propertiesSb.length());
//        }
    }
}
