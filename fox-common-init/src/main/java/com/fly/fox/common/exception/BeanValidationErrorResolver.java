package com.fly.fox.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface BeanValidationErrorResolver {
    Logger logger = LoggerFactory.getLogger(BeanValidationErrorResolver.class);
    
    boolean support(Throwable throwable);
    void resolve(Throwable throwable);
}
