package com.fly.fox.common.exception;

public interface MsgCodeResolver {
    boolean support(Throwable throwable);
    ErrorMsgCode resolve(Throwable throwable);
}
