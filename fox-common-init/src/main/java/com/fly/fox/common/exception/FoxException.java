package com.fly.fox.common.exception;


public class FoxException extends RuntimeException {
    private static final long serialVersionUID = 5539431043384054654L;
    //系统异常消息码
    public static final String SYS_ERROR_MSGCD = ErrorMsgCode.SYS_ERROR.getMsgCd();
    
    private String msgCd;
    private String msgInfo;
    private Throwable cause;
    //是否业务异常，业务代码中只能抛出业务异常
    private boolean businessException = false;
    
    /**
     * @param msgCd
     * @param msgInfo
     * @param cause
     */
    public FoxException(String msgCd, String msgInfo, Throwable cause) {
        super(msgCd+" : "+msgInfo, cause);
        this.msgCd = msgCd;
        this.msgInfo = msgInfo;
        this.cause = cause;
    }
    
    /**
     * @param msgCd
     * @param msgInfo
     */
    public FoxException(String msgCd, String msgInfo) {
        super(msgCd + " :  " + msgInfo);
        this.msgCd = msgCd;
        this.msgInfo = msgInfo;
    }
    
    /**
     * @param errorMsgCode
     */
    public FoxException(ErrorMsgCode errorMsgCode) {
        this(errorMsgCode.getMsgCd(), errorMsgCode.getMsgInfo());
    }
    
    /**
     * @param errorMsgCode
     * @param cause
     */
    public FoxException(ErrorMsgCode errorMsgCode, Throwable cause) {
        this(errorMsgCode.getMsgCd(), errorMsgCode.getMsgInfo(), cause);
    }
    
    /**
     * @param msgCd
     */
    public FoxException(String msgCd) {
        super(msgCd);
        this.msgCd = msgCd;
    }
    
    public FoxException(String msgCd, boolean businessException) {
        super(msgCd);
        this.msgCd = msgCd;
        this.businessException = businessException;
    }
    
    /**
     * @param cause
     */
    public FoxException(Throwable cause) {
        super(SYS_ERROR_MSGCD, cause);
        this.cause = cause;
        this.msgCd = SYS_ERROR_MSGCD;
    }
    
    /**
     * @param msgInfo
     * @param cause
     */
    public FoxException(String msgInfo, Throwable cause) {
        super(SYS_ERROR_MSGCD + " : " + msgInfo);
        this.msgCd = SYS_ERROR_MSGCD;
        this.msgInfo = msgInfo;
        this.cause = cause;
    }
    
    public FoxException() {
        super(SYS_ERROR_MSGCD);
        this.msgCd = SYS_ERROR_MSGCD;
    }
    
    public static void throwBusinessException(String msgCd) {
        throw new FoxException(msgCd, true);
    }
    
    public static void throwLemonException() {
        throw new FoxException();
    }
    
    /**
     * @param msgCd
     */
    public static void throwLemonException(String msgCd) {
        throw new FoxException(msgCd);
    }
    
    /**
     * @param t
     */
    public static void throwLemonException(Throwable t) {
        if(t instanceof FoxException) {
            throw (FoxException)t;
        }
        throw new FoxException(t);
    }
    
    /**
     * @param msgInfo
     * @param t
     */
    public static void throwLemonException(String msgInfo, Throwable t) {
        throw new FoxException(msgInfo, t);
    }
    
    /**
     * @param msgCd
     * @param msgInfo
     */
    public static void throwLemonException(String msgCd, String msgInfo) {
        throw new FoxException(msgCd, msgInfo);
    }
    
    /**
     * @param msgCd
     * @param msgInfo
     * @param throwable
     */
    public static void throwLemonException(String msgCd, String msgInfo, Throwable throwable) {
        throw new FoxException(msgCd, msgInfo, throwable);
    }
    
    /**
     * @param errorMsgCode
     */
    public static void throwLemonException(ErrorMsgCode errorMsgCode) {
        throw new FoxException(errorMsgCode.getMsgCd(), errorMsgCode.getMsgInfo());
    }
    
    /**
     * @param errorMsgCode
     * @param throwable
     */
    public static void throwLemonException(ErrorMsgCode errorMsgCode, Throwable throwable) {
        throw new FoxException(errorMsgCode.getMsgCd(), errorMsgCode.getMsgInfo(), throwable);
    }
    
    /**
     * 创建LemonException
     * 
     * @param t
     * @return
     */
    public static FoxException create(Throwable t) {
        if(isLemonException(t)) {
            return (FoxException) t;
        }
        return new FoxException(t);
    }
    
    public static FoxException create(String msgCd) {
        return new FoxException(msgCd);
    }
    
    public static FoxException create(String msgCd, String msgInfo) {
        return new FoxException(msgCd, msgInfo);
    }
    
    public static FoxException create(ErrorMsgCode errorMsgCode) {
        return new FoxException(errorMsgCode.getMsgCd(), errorMsgCode.getMsgInfo());
    }
    
    public static FoxException create(ErrorMsgCode errorMsgCode, Throwable throwable) {
        return new FoxException(errorMsgCode.getMsgCd(), errorMsgCode.getMsgInfo(), throwable);
    }
    
    public static boolean isLemonException(Throwable throwable) {
        if(throwable!=null&& throwable instanceof FoxException) {
            return true;
        }
        return false;
    }
    
    public String getMsgCd() {
        return msgCd;
    }

    public void setMsgCd(String msgCd) {
        this.msgCd = msgCd;
    }

    public String getMsgInfo() {
        return msgInfo;
    }

    public void setMsgInfo(String msgInfo) {
        this.msgInfo = msgInfo;
    }

    @Override
    public Throwable getCause() {
        return cause;
    }

    public void setCause(Throwable cause) {
        this.cause = cause;
    }

    public boolean isBusinessException() {
        return businessException;
    }

    public void setBusinessException(boolean businessException) {
        this.businessException = businessException;
    }
    
}
