package com.fly.fox.gw;

import com.fly.fox.common.FoxFrameworkInitialization;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringCloudApplication
public class FoxGatewayApplication {

	public static void main(String[] args) {
		FoxFrameworkInitialization.initPlatformEnvAndArgs(args);
		SpringApplication.run(FoxGatewayApplication.class, args);
	}
}
