package com.fly.fox.gw.zuul;

import com.fly.fox.common.FoxConstants;
import com.fly.fox.common.context.FoxContext;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

public class AccessCheckFilter extends ZuulFilter {
	private static final Logger logger = LoggerFactory.getLogger(AccessCheckFilter.class);

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 9;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		logger.debug("接口权限检查,TraceId={}, SpanId={}", FoxContext.getCurrentContext().get(FoxConstants.TRACE_ID),FoxContext.getCurrentContext().get(FoxConstants.SPAN_ID));
		logger.info("===<call 接口权限检查, TraceId={}, SpanId={}>===", request.getHeader("X-B3-TraceId"), request.getHeader("X-B3-SpanId"));
		//FoxContext
		return null;
	}
}
