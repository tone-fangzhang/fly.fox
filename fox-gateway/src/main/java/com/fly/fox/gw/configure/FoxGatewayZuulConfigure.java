package com.fly.fox.gw.configure;

import com.fly.fox.gw.zuul.AccessCheckFilter;
import com.fly.fox.gw.zuul.ContextInitFilter;
import com.fly.fox.gw.zuul.MessageSignCheckFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.sleuth.instrument.zuul.TracePreZuulFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FoxGatewayZuulConfigure {

	@ConditionalOnClass(TracePreZuulFilter.class)
	@Bean
	public ContextInitFilter contextInitFilter() {
		return new ContextInitFilter();
	}

	@Bean
	public AccessCheckFilter accessCheckFilter() {
		return new AccessCheckFilter();
	}

	@Bean
	public MessageSignCheckFilter messageSignCheckFilter() {
		return new MessageSignCheckFilter();
	}
}
